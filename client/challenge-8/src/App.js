// import logo from './logo.svg';
// import './App.css';

import { Routes, Route } from "react-router-dom";
import Form from "./pages/Form";
import List from "./pages/List";

function App() {
  return (
    <Routes>
        <Route path="/" element={<List/>}/>
        <Route path="/form" element={<Form/>}/>
    </Routes>
  );
}

export default App;
