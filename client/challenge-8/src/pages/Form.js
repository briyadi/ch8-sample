import { Component } from "react"

class Form extends Component {
    state = {
        username: "",
        password: "",
        email: "",
        experience: 0,
        lvl: 0,
    }

    onSubmit = (e) => {
        e.preventDefault()
        fetch("http://localhost:5000/api/players", {
            method: "POST",
            headers: {
                "content-type": "application/json"
            },
            body: JSON.stringify(this.state)
        })
    }

    render() {
        return (
            <form onSubmit={this.onSubmit}>
                <input type="text" 
                    onChange={e => this.setState({username: e.target.value})} placeholder="username"/>
                <input type="password" 
                    onChange={e => this.setState({password: e.target.value})} placeholder="password"/>
                <input type="email" 
                    onChange={e => this.setState({email: e.target.value})} placeholder="email"/>
                <button type="submit">Save</button>
            </form>
        )
    }
}

export default Form