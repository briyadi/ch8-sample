import { Component } from "react";
import { Link } from "react-router-dom";

class List extends Component {
    state = {
        players: []
    }

    componentDidMount() {
        fetch("http://localhost:5000/api/players")
            .then(res => res.json())
            .then(data => {
                this.setState({players: [...data.message]})
            })
    }

    renderPlayer = player => {
        return (
            <li key={player.username}>{player.username}</li>
        )
    }

    render() {
        return (
            <>
                <Link to="/form">Create new player</Link>
                <ul>
                    {this.state.players.map(this.renderPlayer)}
                </ul>
            </>
        )
    }
}

export default List